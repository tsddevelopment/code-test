# Coding Test

There are two parts to this test.

The first part is the foundation test - this is designed to test your ability to think responsively. Create a layout that
displays elements in the three sizes shown on the foundation-test file. This file should require very minimal non-foundation
style code. Element 1D is the mobile menu launcher. You may code that however you like, and add any neccessary styling.

The second part of this test is the SCSS test. Recreate the scss-test image in the html file. This file doesn't require use
of any foundation styling or structure. I'm interested in seeing how you build out your html and scss file.

## Build Environment

This test requires having gulp installed on your computer.

In order to get started, open a terminal and inside your project directory run 'npm install' and 'gulp' in that order.
This will install the package dependencies and run a local server from your directory.