/* File: gulpfile.js */

// grab our gulp packages
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    connect = require('gulp-connect');

gulp.task('default', ['watch', 'webserver']);

gulp.task('watch', function () {
    gulp.watch('*.scss', ['build-css']);
    gulp.watch('*.html', ['reload']);
});

gulp.task('webserver', function() {
    connect.server({
        livereload: true
    });
});

gulp.task('reload', function(){
    return gulp.src('**/*.html')
        .pipe(connect.reload());
});

gulp.task('build-css', function () {
    return gulp.src('*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'))
        .pipe(connect.reload());
});

